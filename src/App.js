import React, { Component } from "react";
import "./App.css";
import DefaultScrambler from "./scramblers/default-scrambler";

export default class App extends Component {

  state = {
    text: "",
    scrambled: "",
  }

  onTextChanged = (e) => {
    this.setState({
      text: e.target.value,
    });
  }

  onScrambleClicked = () => {
    const { text } = this.state;
    const scrambled = DefaultScrambler.scramble(text);

    this.setState({ scrambled });
  }

  render() {
    const { text, scrambled } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <h1>Scrambler</h1>
          <a
            className="App-link"
            href="https://gitlab.com/mrstebouk/scramblr"
            target="_blank"
            rel="noopener noreferrer"
          >
            See the code!
          </a>
          <p>
            Enter some text below, and watch it scramble!
          </p>
        </header>
        <main className="App-content">
          <textarea className="App-input" rows={16} cols={100} value={text} onChange={this.onTextChanged} />
          <button className="App-button" disabled={!text} onClick={this.onScrambleClicked}>
            Scramble
          </button>
          {scrambled && (
            <div className="App-scrambled">
              {scrambled}
            </div>
          )}
        </main>
      </div>
    );
  }

}
