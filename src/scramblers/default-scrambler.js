const scrambleWord = (word) => {
  // Only replace the letters
  return word.replace(/[a-zA-Z]+/g, (m) => {
    // Turn the string into an array of characters
    const array = m.split("");

    // Create a loop that is the size of the word
    return [...Array(word.length).keys()].reduce((r) => {
      const index = Math.floor(Math.random() * array.length);

      // Pulls the letter out of the array, and also mutates the array variable
      return r + array.splice(index, 1);
    }, "");
  });
};

export default {
  scramble: (text) => {
    return text.split(" ").map(scrambleWord).join(" ");
  },
};
