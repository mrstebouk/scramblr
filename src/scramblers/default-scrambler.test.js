import DefaultScrambler from "./default-scrambler";

describe("DefaultScrambler", () => {
  it("scrambles text", () => {
    const result = DefaultScrambler.scramble("Hello World");

    expect(result).not.toBe("Hello World");
    expect(result).toMatch(/^[Helo]+ [World]+$/);
  });

  it("handles punctuation", () => {
    const result = DefaultScrambler.scramble("Hello, World!");

    expect(result).not.toBe("Hello, World!");
    expect(result).toMatch(/^[Helo]+, [World]+!$/);
  });
});
